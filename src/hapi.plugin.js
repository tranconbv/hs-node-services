const logger = require('./logger.js');
/* eslint-disable no-param-reassign */

exports.register = function (server, options, next) {
    server.ext('onRequest', (hapiRequest, reply) => {
        // hapi request model: https://hapijs.com/api#request-object
        const url = (hapiRequest.headers['x-forwarded-proto'] || hapiRequest.connection.info.protocol)
            + '://' + hapiRequest.info.host + hapiRequest.url.path;
        hapiRequest.stackDriver = {
            httpContext: {
                // id: req.id,
                requestMethod: hapiRequest.method.toUpperCase(),
                requestUrl: url,
                userAgent: hapiRequest.headers['user-agent'],
                remoteIp: hapiRequest.info.remoteAddress,
                referer: hapiRequest.info.referrer
            },
        };
        hapiRequest.logger = hapiRequest.stackLogger  = new Proxy(logger.logger, {
            get: (target, propName) => {

                var item = target[propName];
                if(typeof item !== 'function'){
                    return;
                }
                // return a wrapper for all (log) methods
                return function() {
                    let args = [...arguments];
                    if(args.length == 1) 
                        args = [{}, args[0]];
                    args[0].correlationId = hapiRequest.id;
                    args.push(hapiRequest.stackDriver.httpContext);
                    return item.apply(target, args);
                };
            }
        });
        return reply.continue();
    });

    server.ext('onPreResponse', (hapiRequest, reply) => {
        const res = hapiRequest.response;
        if (res.isBoom) {
            // https://www.npmjs.com/package/boom
            const httpContext = hapiRequest.stackDriver.httpContext;
            logger.logger.error(
                { origin: 'hapi_onPreResponse' },
                {
                    message: res.stack || res.message, //get default Boom message if no error stack provided
                    context: {
                        httpContext: {
                            url: httpContext.requestUrl,
                            method: httpContext.requestMethod,
                            userAgent: httpContext.userAgent,
                            referrer: httpContext.referer,
                            responseStatusCode: res.output.payload.statusCode,
                            remoteIp: httpContext.remoteIp
                        },
                        user: hapiRequest.auth.credentials && hapiRequest.auth.credentials.email ? hapiRequest.auth.credentials.email : '',
                        serviceContext: {
                            service: logger.logger.config.Name,
                            version: logger.logger.config.AppVersion || 'default'
                        }
                    }
                },
                httpContext);
        }
        return reply.continue();
    });
    next();
};

exports.register.attributes = {
    name: 'HapiStackDriverLogger',
    version: '1.0.0'
};