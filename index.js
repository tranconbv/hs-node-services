const logger = require('./src/logger.js');
const config = require('./src/config.ext.js');
const HapiLoggerPlugin = require('./src/hapi.plugin.js');

module.exports = {
    logger: logger.logger,
    LoggerClass: logger.Class,
    config: config.config,
    configFactory: config.factory,
    HapiLoggerPlugin
};
